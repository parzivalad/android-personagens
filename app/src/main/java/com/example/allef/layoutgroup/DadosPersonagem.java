package com.example.allef.layoutgroup;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.List;

/**
 * Activity de exibição das informações do personagem
 */
public class DadosPersonagem extends AppCompatActivity
{
    /** TAG para o Log no console */
    private static final String TAG = "DADOS personagens";

    static List<String> personagens = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dados_personagem);

        telaDadosPersonagem();
    }

    /**
     * Altera os TextViews para mostrar os dados dos personagens
     */
    private void telaDadosPersonagem()
    {
        String[] dadosPersonagem = getDadosPersonagem();

        // Pegando os TextViews e tranformando em objetos
        TextView txtVNome = (TextView) findViewById(R.id.txtVNome);
        TextView txtVIdade = (TextView) findViewById(R.id.txtVIdade);
        TextView txtVClasse = (TextView) findViewById(R.id.txtVClasse);
        TextView txtVUsarEscudo = (TextView) findViewById(R.id.txtVUsarEscudo);

        // Mudando os valores dos TextViews pelos objetos
        txtVNome.setText("Sr. " + dadosPersonagem[1]);
        txtVIdade.setText("Você tem " + dadosPersonagem[2] + " anos");
        txtVClasse.setText("E é um " + dadosPersonagem[3]);
        txtVUsarEscudo.setText("Sr. " + dadosPersonagem[1] + ", você " + dadosPersonagem[4]);
    }

    /**
     * Pega os dados do personagem enviados pela Activity anterior
     * @return dadosPersonagem[]: String
     */
    private String[] getDadosPersonagem()
    {
        //Recebendo dados da tela anterior
        Intent intent = getIntent();
        Log.v(TAG, intent.getStringExtra("id"));
        int id = Integer.parseInt(intent.getStringExtra("id"));
        String[] dadosPersonagem = Personagem.INSTANCE.getListPersonagem().get(id);
        return dadosPersonagem;
    }

    private Context getContext()
    {
        return this;
    }
}
