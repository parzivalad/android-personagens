package com.example.allef.layoutgroup;

import android.content.Context;
import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Activity principal
 */
public class MainActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnCriarPersonagem();
        btnListarPersonagens();
    }

    /**
     * Redireciona para a Activity de Criar Personagem
     */
    private void btnCriarPersonagem()
    {
        Button btn = (Button) findViewById(R.id.btnCriarPersonagem);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getContext(), CriarPersonagem.class);
                startActivity(i);
            }
        });
    }

    /**
     * Redireciona para a Activity de Listar os Personagens criados
     */
    private void btnListarPersonagens()
    {
        Button btn = (Button) findViewById(R.id.btnListarPersonagens);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getContext(), ListPersonagens.class);
                startActivity(i);
            }
        });
    }

    private Context getContext()
    {
        return this;
    }
}
