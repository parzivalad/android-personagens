package com.example.allef.layoutgroup;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

/**
 * Classe de criação/cadastro de personagem
 */
public class CriarPersonagem extends AppCompatActivity
{
    /** TAG para o Log no console */
    private static final String TAG = "Criando personagem";
    /** Referente ao campo input do nome do personagem */
    private EditText nomePersonagem;
    /** Referente ao campo input da idade do personagem */
    private EditText idadePersonagem;
    /** Referente ao tipo "Mago" do personagem */
    private RadioButton rbMago;
    /** Referente ao tipo "Guerreiro" do personagem */
    private RadioButton rbGuerreiro;
    /** Define se o personagem vai usar escudo ou não */
    private CheckBox cbEscudo;
    /** Intent de redirecionamento de activity */
    private Intent i;
    /** Guarda os dados do personagem */
    private String[] dadosPersonagem;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_criar_personagem);

        EditText nomePersonagem = (EditText) findViewById(R.id.nomePersonagem);
        nomePersonagem.requestFocus();

        btnCriarPersonagem();

        Log.v(TAG, "Personagem criado!");
    }

    /**
     * Executa a ação de cadastro
     * Muda para a tela de dados do personagem se tudo for preenchido
     */
    private void btnCriarPersonagem()
    {
        Button btn = (Button) findViewById(R.id.btnCriarPersonagem);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i = new Intent(getContext(), ListPersonagens.class);
                dadosPersonagem = getDadosPersonagem();
                if(null != dadosPersonagem) {
                    AlertDialog dialog = alertOk().create();
                    dialog.show();
                }
            }
        });
    }

    /**
     * Pega os valores dos imputs e adiciona a um array e String
     * @return dadosPersonagem[]: String
     */
    private String[] getDadosPersonagem()
    {
        // Array com as informações do personagem
        String[] dadosPersonagem = new String[5];
        // Classe do personagem (mago ou guerreiro)
        String classe = "";
        // Se o personagem utiliza escudo
        String usarEscudo = "não usa escudo";

        getValuesInput();

        // Verificando classe selecionada
        if (this.rbMago.isChecked()) {
            classe = "Mago";
        } else if (this.rbGuerreiro.isChecked()) {
            classe = "Guerreiro";
        } else {
            return null;
        }

        if(this.cbEscudo.isChecked()) {
            usarEscudo = "usa escudo";
        }

        // Adicionando informações ao array
        dadosPersonagem[1] = this.nomePersonagem.getText().toString();
        dadosPersonagem[2] = this.idadePersonagem.getText().toString();
        dadosPersonagem[3] = classe;
        dadosPersonagem[4] = usarEscudo;

        // Teste para campos vazios
        for (int i = 1; i < dadosPersonagem.length; i++) {
            if(dadosPersonagem[i].isEmpty()) {
                return null;
            }
        }

        Personagem.INSTANCE.id++;
        dadosPersonagem[0] = Integer.toString(Personagem.INSTANCE.id);
        Personagem.INSTANCE.addInList(dadosPersonagem);

        return dadosPersonagem;
    }

    /**
     * Pega os inputs da activity e e adiciona aos objetos globais
     */
    private void getValuesInput()
    {
        this.nomePersonagem = (EditText) findViewById(R.id.nomePersonagem);
        this.idadePersonagem = (EditText) findViewById(R.id.idadePersonagem);
        this.rbMago = (RadioButton) findViewById(R.id.rbMago);
        this.rbGuerreiro = (RadioButton) findViewById(R.id.rbGuerreiro);
        this.cbEscudo = (CheckBox) findViewById(R.id.cbEscudo);
    }

    public AlertDialog.Builder alertOk() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Sucesso");
        builder.setMessage("Personagem criado com sucesso!");

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(CriarPersonagem.this,  dadosPersonagem[3] + " criado!", Toast.LENGTH_SHORT).show();
                startActivity(i);
                return;
            }
        });

        return builder;
    }

    private Context getContext()
    {
        return this;
    }
}
