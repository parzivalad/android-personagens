package com.example.allef.layoutgroup;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Activity para listar os personagens criados
 */
public class ListPersonagens extends AppCompatActivity
{
    /** TAG para o Log no console */
    private static final String TAG = "Listar personagens";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_personagens);

        /** Referente ao elemento/view com a lista dos personagens */
        ListView listaPersonagens = (ListView) findViewById(R.id.listItens);
        /** Adaptador com a lista dos personagens */
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, getPersonagem());
        /** Conecta o adaptador à view de lista dos personagens */
        listaPersonagens.setAdapter(adapter);

        listViewClick();
    }

    /**
     * Busca os personagens no Enum Personagens e retorna os nomes dos mesmos
     * @return listPersonagens: List<String>
     */
    private List<String> getPersonagem()
    {
        List<String> listPersonagens = new ArrayList<>();
        List<String[]> personagens = Personagem.INSTANCE.getListPersonagem();

        int sizeList = personagens.size();

        if (sizeList > 0) {
            for (int i = 0; i < sizeList; i++) {
                listPersonagens.add(personagens.get(i)[0]);
            }
        }

        return listPersonagens;
    }

    /**
     * Verifica o item clicado e redireciona para a Activity DadosPersonagem com o id do Item
     */
    private void listViewClick()
    {
        ListView listview = (ListView) findViewById(R.id.listItens);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.v(TAG, "Foi clicado na posição " + position + " com o ID " + id );
                Intent intent = new Intent(getContext(), DadosPersonagem.class);
                intent.putExtra("id", Long.toString(id));
                startActivity(intent);
            }
        });
    }

    private Context getContext()
    {
        return this;
    }
}
