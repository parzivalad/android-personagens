package com.example.allef.layoutgroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Esse Enum representa a entidade Personagem
 * Created by Allef on 28/04/2018.
 */
public enum Personagem
{
    INSTANCE;
    /** Representa o id de cada personagem */
    int id;
    /** Lista com todos os personagens criados */
    private List<String[]> listPersonagem;

    Personagem()
    {
        id = 0;
        listPersonagem = new ArrayList<>();
    }

    /**
     * Retorna uma lista com os personagens
     * @return listPersonagem
     */
    public List<String[]> getListPersonagem()
    {
        return listPersonagem;
    }

    /**
     * Altera a lista completa dos personagens
     * @param listPersonagem
     */
    public void setListPersonagem(List<String[]> listPersonagem)
    {
        this.listPersonagem = listPersonagem;
    }

    /**
     * Adiciona um novo personagem à lista
     * @param personagem
     */
    public void addInList(String[] personagem)
    {
        this.listPersonagem.add(personagem);
    }
}
